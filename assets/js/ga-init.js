"use strict";

window.dataLayer = window.dataLayer || [];
function gtag(){
  dataLayer.push(arguments);
}
gtag("js", new Date());

gtag("config", "UA-1985327-5");
